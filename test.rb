require "em-synchrony"
require "em-synchrony/em-http"
require 'benchmark'

class Test
  TOTAL_REQUESTS= 8
  def self.get
    processed = []
    EventMachine.synchrony do
      TOTAL_REQUESTS.times do |i|
        # run "unicorn --config=config/unicorn.rb" to start local 4-threaded server
        conn = EventMachine::HttpRequest.new("http://127.0.0.1:8080/hi/#{i}")
        # conn = EventMachine::HttpRequest.new('http://google.com/')
        http = conn.get
        http.errback &errback(processed)
        http.callback &callback(processed)
      end
    end
  end
  
  def self.aget
    processed = []

    EventMachine.synchrony do
      TOTAL_REQUESTS.times do |i|
        conn = EventMachine::HttpRequest.new("http://127.0.0.1:8080/hi/#{i}")
        # conn = EventMachine::HttpRequest.new('http://google.com/')
        http = conn.aget
        http.errback &errback(processed)
        http.callback &callback(processed)
      end
    end
  end
  
  def self.callback(processed)
    Proc.new {
      processed << 1
      # p http.response_header.status
      # p http.response
      if processed.size == TOTAL_REQUESTS
        # puts 'done'
        EventMachine.stop          
      end
    }
  end
  
  def self.errback(processed)
    Proc.new { |r| 
      processed << 1
      p r.error
      if processed.size == TOTAL_REQUESTS
        EventMachine.stop          
      end
    }
  end
end

Benchmark.bm(5) do |x|
  x.report('aget:') { Test.aget }
  x.report('get: ') { Test.get  }
end
