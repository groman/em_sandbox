Open terminal and run ``unicorn --config=config/unicorn.rb``. This would start simple server with 4 threads and 300ms response time.

In second terminal run ``ruby test.rb``. Script sends 8 requests to the server.

Sample output


```
            user     system      total        real
aget:   0.000000   0.010000   0.010000 (  0.651168)
get:    0.010000   0.000000   0.010000 (  2.439173)
```