require 'celluloid'
require 'net/http'
require 'benchmark'
require 'pry'

TOTAL_REQUESTS = 10

class HttpRequestClient
  def get(url:, params:)
    uri       = URI(url)
    uri.query = URI.encode_www_form(params)
    response  = Net::HTTP.get_response(uri)
  end
end


class HttpRequestsPool
  POOL_SIZE = 4

  def initialize
    env_size  = ENV['REQUESTS_POOL_SIZE'].to_s.to_i
    pool_size = env_size > 0 ? env_size : POOL_SIZE
    @pool = HttpRequestActor.pool(size: pool_size)
  end

  def get(url:, params:)
    future_get = @pool.future.get(url: url, params: params)
    HttpFutureRequestWrapper.new(future_get)
  end
end

class HttpRequestActor
  include Celluloid

  def get(url:, params:)
    HttpRequestClient.new.get(url: url, params: params)
  end
end

class HttpFutureRequestWrapper
  def initialize(future_get)
    @future = future_get
  end

  def response
    @future.value
  end
end

Celluloid.task_class = Celluloid::TaskThread

pool = HttpRequestsPool.new
# pool = HttpRequestClient.new
Benchmark.bm(1) do |x|
  x.report do
    clients = (1..TOTAL_REQUESTS).to_a.map do |i|
      client = pool.get(url: "http://localhost:8080/hi/#{i}", params: {})
    end
    # binding.pry
    pp clients.map{|c| c.response.code }
  end
end

